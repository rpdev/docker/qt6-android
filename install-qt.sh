#!/bin/bash

set -e

BASEDIR="$(cd "$(dirname "$0")" && pwd)"

QT_VERSION_JS="$(echo $QT_VERSION | sed -e s/\\.//g)"

cd "$BASEDIR"
QT_INSTALL_DIR=/opt/Qt

if [ ! -f qt-unified-linux-x64-online.run ]; then
    URL="$QT_INSTALLER_URL"
    curl -s -L -o qt-unified-linux-x64-online.run $URL
fi

chmod +x qt-unified-linux-x64-online.run

PACKAGES=$(./qt-unified-linux-x64-online.run \
    search \
        --email $QT_CI_LOGIN  \
        --password $QT_CI_PASSWORD \
        --no-save-account \
        "qt.qt6.$QT_VERSION_JS.(addons|android|linux_gcc_64)" | \
    grep 'name=' | \
    awk '{ print $2; }' | \
    cut -d '"' -f 2 \
)
rm -rf /root/.cache/qt-unified-linux-online
./qt-unified-linux-x64-online.run \
    install \
    --email $QT_CI_LOGIN \
    --password $QT_CI_PASSWORD \
    --no-save-account \
    --accept-messages \
    --confirm-command \
    --accept-obligations \
    --accept-licenses \
    --auto-answer message.id=Ok \
    --no-force-installations \
    --no-default-installations \
    --root $QT_INSTALL_DIR \
    $PACKAGES
