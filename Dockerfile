FROM fedora

# Hint: Look up Android SDK versions from here - https://doc.qt.io/qt-6/android-getting-started.html
ARG SDK_URL="https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip"
ARG ANDROID_NATIVE_API_LEVEL=34
ARG SDK_ROOT="/opt/android/android-sdk"
ARG SDK_PLATFORM=android-${ANDROID_NATIVE_API_LEVEL}
ARG SDK_BUILD_TOOLS=${ANDROID_NATIVE_API_LEVEL}.0.0
ARG SDK_PACKAGES="tools platform-tools"
ARG NDK_VERSION=26.1.10909125
ARG QT_INSTALLER_URL=http://download.qt.io/official_releases/online_installers/qt-unified-linux-x64-online.run
ARG QT_VERSION=6.8.1
ARG QT_CI_LOGIN
ARG QT_CI_PASSWORD

ADD install-qt.sh /root/
RUN \
    dnf install -y \
        bison \
        cmake \
        curl \
        extra-cmake-modules \
        flex \
        fontconfig \
        gcc \
        gcc-c++ \
        git \
        java-17-openjdk \
        java-17-openjdk-devel \
        libX11 \
        libX11-xcb \
        libXext \
        libxkbcommon-x11 \
        libXrender \
        make \
        mesa-libGL \
        ninja-build \
        openssh-clients \
        rpm-build \
        ruby-devel \
        unzip \
        wget \
        which \
        xcb-util-image \
        xcb-util-keysyms \
        xcb-util-renderutil \
        xcb-util-wm \
        && \
    curl -s "https://get.sdkman.io" | bash && \
    source "$HOME/.sdkman/bin/sdkman-init.sh" && \
    sdk install gradle 8.10 && \
    mkdir -p $SDK_ROOT && \
    cd $SDK_ROOT && \
    curl -s -o commandlinetools.zip https://dl.google.com/android/repository/commandlinetools-linux-9123335_latest.zip && \
    unzip *.zip && \
    rm *.zip && \
    cd .. && \
    (sleep 5 ; while true ; do sleep 1 ; printf 'y\r\n' ; done ) | \
        ./android-sdk/cmdline-tools/bin/sdkmanager --update --sdk_root=$SDK_ROOT && \
    (sleep 5 ; while true ; do sleep 1 ; printf 'y\r\n' ; done ) | \
        ./android-sdk/cmdline-tools/bin/sdkmanager "platforms;${SDK_PLATFORM}" "ndk;${NDK_VERSION}" "build-tools;${SDK_BUILD_TOOLS}" ${SDK_PACKAGES} --sdk_root=$SDK_ROOT && \
    dnf clean all
RUN bash /root/install-qt.sh


ENV ANDROID_SDK_ROOT=$SDK_ROOT
ENV ANDROID_NDK=$SDK_ROOT/ndk/$NDK_VERSION
ENV ANDROID_NDK_ROOT=$SDK_ROOT/ndk/$NDK_VERSION
ENV JAVA_HOME=/usr/lib/jvm/java-17
ENV QT_VERSION=$QT_VERSION
ENV QT_INSTALL_ROOT=/opt/Qt
